.. NxSkinBR documentation master file, created by
   sphinx-quickstart on Fri Apr 15 11:47:36 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bem-vindo ao Tutorial do NxFilter!
====================================

.. warning::

   Esta documentação será descontinuada. 
   
   Durante muito tempo esta tradução não oficial do NxFilter ajudou muitas pessoas, serviu de apoio a diversos brasileiros e países em que a línguá oficial seja a portugues. Somado a isso contribuímos com a comunidade, mas atualmente existe uma tradução oficial, mantida pela empresa detentora do software e atualizada constantemente a qual pode ser acessada em <https://nxfilter.org/tutorial/a-why-nxfilter.php?locale=pt>

Conteúdo:

.. toctree::
   :maxdepth: 2

   pages/getting_started
   pages/jahasfilter-what
   pages/whatablacklist
   pages/authentication
   pages/gui_overview
   pages/working_agent
   pages/nxcloud
   pages/custom_before
   pages/nxclassifier
   pages/dns_server
   pages/misc
   pages/faq
