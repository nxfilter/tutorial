# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2018, Kernel - Tecnologia da Informação
# This file is distributed under the same license as the NxFilter - Tutorial
# - BR package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2019.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: NxFilter - Tutorial - BR 0.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-03-23 11:32-0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.4.0\n"

# 2d276c58b2a448378eab5a0bf53f3f94
#: ../../pages/getting_started/sys-req.rst:2
msgid "Recursos Mínimos Necessários"
msgstr ""

# 17c8f9bdbe5a48c3ad561188e174ee3b
#: ../../pages/getting_started/sys-req.rst:4
msgid ""
"Windows, Linux, FreeBSD ou qualquer outro OS que tenha suporte a Java 7 "
"ou 8."
msgstr ""

# c33a6c79b6614980b6a2184ba5b85f24
#: ../../pages/getting_started/sys-req.rst:5
msgid "768 MB RAM."
msgstr ""

# 3d0b5d7d143d4e11a56f798f076e3cd9
#: ../../pages/getting_started/sys-req.rst:6
msgid "4 GB de espaço disponível."
msgstr ""

# 5191c9a5c33f47e6a881c1e24a6be2a4
#: ../../pages/getting_started/sys-req.rst:7
msgid "Portas UDP/53, TCP/80, TCP/443."
msgstr ""

# 55e076350423405b8d458dea587b9fe4
#: ../../pages/getting_started/sys-req.rst:11
msgid ""
"OpenJDK 9 é incompatível com o servidor Web do sistema. É recomendado o "
"uso do OpenJDK 8 para casos de uso do NxFilter com o OpenJDK."
msgstr ""

# 8c9c34f9900940d2aecffdc4ea9524d1
#: ../../pages/getting_started/sys-req.rst:13
msgid ""
"Por padrão, o NxFilter usa 768 MB de memória. Esse é o requisito mínimo, "
"então para ambientes com centenas de usuários isso pode não ser o "
"bastante. Para saber como alocar mais recursos acesse: :doc:`Performance "
"<../misc/performance>`"
msgstr ""

# 8c9c34f9900940d2aecffdc4ea9524d1
#~ msgid ""
#~ "Por padrão, o NxFilter usa 768 MB"
#~ " de memória. Esse é o requisito "
#~ "mínimo, então para ambientes com "
#~ "centenas de usuários isso pode não "
#~ "ser o bastante. Para saber como "
#~ "alocar mais recursos acesse "
#~ ":doc:`../misc/performance.rst`"
#~ msgstr ""

