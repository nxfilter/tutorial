# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2018, Kernel - Tecnologia da Informação
# This file is distributed under the same license as the NxFilter - Tutorial
# - BR package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2019.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: NxFilter - Tutorial - BR 0.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-03-23 10:26-0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.4.0\n"

# 1fcf96e1e8a047caa949f9aa56697c80
#: ../../pages/working_agent/agent_application_control.rst:3
msgid "Controlando aplicações com NxClient"
msgstr ""

# f3817f6d19b04b999da48d26b34c3b4e
#: ../../pages/working_agent/agent_application_control.rst:5
msgid ""
"O NxFilter permite que se controle as aplicações executadas no desktop "
"com o agente NxClient. Você pode bloquear as aplicações desejadas "
"configurando a política de controle de aplicações através da GUI do "
"NxFilter - de modo centralizado - e ainda visualizar, quem tentou "
"executar os programas bloqueados, através dos logs."
msgstr ""

# b58c6f4654664eb6979fef9c989c0a28
#: ../../pages/working_agent/agent_application_control.rst:9
msgid "Como funciona?"
msgstr ""

# 20065fadc528478cbdda3d986eb3c919
#: ../../pages/working_agent/agent_application_control.rst:11
msgid ""
"Após definir a sua política de controle de aplicações em ``Policy & Rule "
"> Application Control`` o Nxclient obtém essas definições de tempos em "
"tempos."
msgstr ""

# 3ee09b93eeb248038afa98bb256b7652
#: ../../pages/working_agent/agent_application_control.rst:15
msgid ""
"Você pode ajustar o tempo de atualização alterando os valores no "
"parâmetro ''Agent Policy Update Period'' em ``Config > Setup``."
msgstr ""

# c1bf0281584d4cd0a823dafac8599e6f
#: ../../pages/working_agent/agent_application_control.rst:19
msgid "Opções suportadas"
msgstr ""

# f12bd7c826d2470ba275ac1c82815d9b
#: ../../pages/working_agent/agent_application_control.rst:21
msgid "Bloquear a busca de portas"
msgstr ""

# 5b69b3006687411a930d910717f58886
#: ../../pages/working_agent/agent_application_control.rst:23
msgid ""
"NxClient detecta processos do UltraSurf e Tor através de verificação de "
"portas. Isso significa que mesmo que os usuários mudem o nome do "
"aplicativo ou os execute através de um pen drive é possível encontrá-los "
"e bloqueá-los."
msgstr ""

# 237bd1bcc08d48209e3a1d32f225d095
#: ../../pages/working_agent/agent_application_control.rst:25
msgid "Bloquear por nome do processo"
msgstr ""

# 95f1517f657e446198dab47666227d0d
#: ../../pages/working_agent/agent_application_control.rst:27
msgid ""
"Você pode bloquear um processo por um determinado nome. Funciona baseado "
"na identificação de palavras chave. Você pode adicionar as palavras chave"
" na GUI e se o agente identificar algum processo com esse nome ele será "
"bloqueado."
msgstr ""

# 55bffcb220564769bf76723c6d694587
#: ../../pages/working_agent/agent_application_control.rst:30
msgid "Habilitar o controle de aplicações apenas para determinados usuários"
msgstr ""

# 8661e29662cc47ebaf879563c62a20e9
#: ../../pages/working_agent/agent_application_control.rst:32
msgid ""
"Partimos de um principio em que o controle de aplicações é uma política "
"global. Porém se houver necessidade de aplicá-las a apenas determinados "
"usuários, altere o parâmetro ''Disable Application Control'' em ``Policy "
"& Rule > Policy > EDIT``."
msgstr ""

# da1c5f20e1434d1691a27a99d2ebf59f
#: ../../pages/working_agent/agent_application_control.rst:35
msgid "Registrando o bloqueio de aplicações"
msgstr ""

# d85d844ba6b64768922520918de65189
#: ../../pages/working_agent/agent_application_control.rst:37
msgid ""
"NxFilter é, em sua essência, um filtro DNS então o formato de registro de"
" logs foi preparado para exibir domínio bloqueados/permitidos. Para poder"
" registrar dados sobre aplicações bloqueadas o NxFilter insere os "
"seguintes domínios ou regras."
msgstr ""

# ea5fbd5feaef44c988e0b5911903ea49
#: ../../pages/working_agent/agent_application_control.rst:39
msgid "ultrasurf.port.app : UltraSurf foi bloqueado por verificação de portas."
msgstr ""

# e0cd36aedfb541b5affc2d4d0a6731f5
#: ../../pages/working_agent/agent_application_control.rst:40
msgid "tor.port.app : Tor foi bloqueado por verificação de portas."
msgstr ""

# 4330846c72b849aaacc80a2d6e57a85c
#: ../../pages/working_agent/agent_application_control.rst:41
msgid "chrome.exe.pname.app : Chrome foi bloqueado através do nome do processo."
msgstr ""

# d4067f23b5864536aed70c8b65e3f62c
#: ../../pages/working_agent/agent_application_control.rst:42
msgid "Skype.title.app : Skype foi bloqueado com base no título da aplicação."
msgstr ""

# 53ca4b2af0724277a5d79b869f60ecc6
#: ../../pages/working_agent/agent_application_control.rst:46
msgid ""
"Quando você ativa o parâmetro ''Block UltraSurf'' e existem extensões do "
"UltraSurf no Chrome ou há outras extensões para proxy instalados, o "
"NxClient mata o processo e registra o sinal ''ultrasurf.chrome.app''."
msgstr ""

# 530e1e677f614ee6879c4a0b6bb74a2f
#: ../../pages/working_agent/agent_application_control.rst:49
msgid "Intervalo de execução"
msgstr ""

# fbbc273bce2447d7a9d553a2d1e2c1bf
#: ../../pages/working_agent/agent_application_control.rst:51
msgid ""
"Encontrar e bloquear aplicações pode causar uma certa carga no "
"processamento. Se você não deseja causar um grande impacto na carga do "
"PC/Desktop do usuário, incremente o valor no parâmetro ''Execution "
"Interval'' em ``Policy & Rule > Application Control``."
msgstr ""

