.. _sso_cxlogon:

********************************************
Single Sign-On usando CxLogon
********************************************

CxLogon é o mais novo agente para SSO do NxFilter, funciona a partir da versão 4.3 do NxFilter. Ao contrário do NxLogon ou NxMapper, não é necessário inicializá-lo com uma GPO ou deixá-lo rodando no controlador de domínio ( servidor AD ). Só é preciso instalá-lo no usuário do sistema e ele criará a sessão no NxFilter com o usuário registrado no sistema. A vantagem desse recurso é que desobriga a existência do AD para se ter o recursos de SSO.

.. note:: 
  Mas não impede o uso do NxFilter intragrado ao AD.

Funcionamento
^^^^^^^^^^^^^^

Assim como os outros agentes para SSO, é necessário criar um usuário no NxFilter antes de usar o CxLogon. Porém, ao contrário dos outros agentes, ele criará uma requisição de acesso caso não encontre o usuário registrado no sistema em que ele foi executado. Você pode aprovar tal requisição em 'User > Login Request' e um usuário será criado automaticamente.

.. note::
  O NxFilter tem de ser o único serviço de DNS utilizado pela estação/terminal na sua rede. Caso contrário o CxLogon não conseguirá encontrar o servidor NxFilter.


CxLogon no Windows
^^^^^^^^^^^^^^^^^^

Ao instalar o sistema em um PC ele entrará em modo de execução como um serviço. Se deseja instalar em diversas estações isso pode ser feito através de uma GPO no AD usando o pacote MSI disponibilizado.


CxLogon no MAC 
^^^^^^^^^^^^^^^

Existe um instalador para o Mac OS. Ao instalá-lo, ele criará o diretório '/Library/cxlogon' e o arquivo '/Library/LauachDaemon/org.nxfilter.cxlogon.plist' para que o mesmo seja inicializado automaticamente.

Para verificar seu funcionamento, faça

.. code-block:: bash

 sudo /Library/cxlogon/bin/test.sh

Iniciar e parar o serviço manualmente:

.. code-block:: bash

  #Iniciar
  sudo /bin/launchctl load -w /Library/LaunchDaemons/org.nxfilter.cxlogon.plist
  #Para
  sudo /bin/launchctl unload -w /Library/LaunchDaemons/org.nxfilter.cxlogon.plist

Desinstalar o sistema
.. code-block:: bash

  sudo /Library/cxlogon/uninstall-mac.sh

CxLogon no Chromebook
^^^^^^^^^^^^^^^^^^^^^^^

Para o Chrome há uma extensão do CxLogon para os usuários do Chromebook. É importante destacar que por conta disso o sistema só roda quando o Chrome é aberto. Isso pode causar um certo incomodo, para contornar isso é possível ainda criar um usuário padrão vinculado a uma faixa de IPs e atribuir uma política com privilégios básicos com acesso a determinados sites.

- CxLogon na Web Store 
.. _`Download do CxLogon para Chromebook na Chrome Web Store`: https://chrome.google.com/webstore/detail/cxlogon/amimdppejnggjhnijjmnkbecbdifnbfa

