Integração com o AD
^^^^^^^^^^^^^^^^^^^

O NxFilter oferece integração com o AD, porém algumas pessoas acham complexo o seu entendimento. Por esse motivo nessa parte do tutorial explicaremos o funcionamento da integração com o NxFilter, quando usá-lo e como implementá-lo no nível conceitual.

Mas o que vem a ser a integração com o AD?
------------------------------------------

Um dos motivos do interesse em uso do NxFilter com AD é para que se possa aplicar as políticas de restrição baseadas no usuários e grupos do AD. E também evitar que se tenha de criar mais uma base de usuários só para controlar o uso de internet, exceto casos em que eles se autentiquem em seu próprio computador. Para o NxFilter, 'integração com AD' significa usar a mesma conta de usuário existente no AD para distinguir os níveis de acesso dos usuários e ter o SSO usando o AD da empresa.

Importando os usuários
----------------------

Tendo o conhecimento do que é a 'integração com AD' e o motivo para usá-la, será explicado agora como usá-la. A primeira coisa a se fazer para ativar esse recurso é importar os usuários e grupos do AD. O que significa que é necessário permitir que o NxFilter tome conhecimento dos usuários e grupos. Para fazer isso basta acessar o menu 'User & Group > Active Directory'.

Após a importação de usuários e grupos, seus usuários estará aptos a se autenticar no NxFilter utilizando suas credenciais registradas no AD. Então foi alcançado a integração do NxFilter com o AD.

Single sign-on (SSO) com Active Directory (AD)
----------------------------------------------

Pode-se dizer que aplicando os passos acima já se tem integração com o AD. Porém, em alguns casos, não é interessante para o usuário ter mais uma página de autenticação pedindo suas credenciais, por isso o próximo passo é implementar o SSO com o AD.

Para implementar o SSO é necessário lançar mão do uso de um agente trabalhando com o NxFilter. Existem diversos :ref:`agentes <working_diff>` no NxFilter que permitem tal funcionalidade, são NxLogon, NxMapper e  NxClient. Você pode usar um ou mais de modo que se complementem e que atendam sua necessidade.

.. note::

  Para mais informações, leia sobre o SSO e os Agentes do NxFilter neste mesmo tutorial

Servidor MS DNS e NxFilter
***************************

Ao colocar o NxFilter em um ambiente com AD pode haver a preocupação acerca da possibilidade de se quebrar a integrade do AD já que o NxFilter é um servidor DNS e o papel do Servidor DNS no AD é muito importante. Mas não se tem o objetivo de desativar ou substituir o servidor DNS do AD. A abordagem correta é fazer com que o NxFilter trabalhe em conjunto com o servidor DNS do AD.

Para manter o funcionamento do Servidor MS DNS em conjunto com o NxFilter, os passos são:

 #. Onde instalar ?
     Como já existe o servidor MS DNS, não se deve instalar o NxFilter dentro do servidor AD, isso causaria conflito nos serviços. O correto é instalar o NxFilter em outro servidor para evitar tal conflito.
 #. Atualização dinâmica dos Desktops/Dispositivos
     O Servidor DNS no AD tem diversas obrigações. Permite que os clientes do AD tenham conhecimento de seus recursos através dos registros SRV. E mantém uma zona DNS para cada cliente. Ainda atualiza dinâmicamente o relacionamento cliente <-> IP, informando as alterações do endereço IP. Para que isso tudo continue funcionando o NxFilter faz um bypass para consultas DNS ao domínio do AD para servidor MS DNS automaticamente. Assumindo que os usuários já foram importados.
 #. Qual o servidor DNS Upstream usado no NxFilter?
     É comum a duvida sobre que servidor DNS deve ser usado como Upstream no NxFilter, principalmente quando já tem um servidor DNS na rede como o MS DNS do AD. Pode ser usado como Upstream qualquer servidor DNS no NxFilter, inclusive o MS DNS. NXFilter continuará direcionando as consultas internas para seu AD. Então pode usar como upstream qualquer servidor DNS que achar melhor, desde que ele consiga retornar os domínios existentes na internet também.
 #. Configuração manual do servidor MS DNS.
     Após a importação de usuários e grupos do AD, o NxFilter tentará trabalhar com seu servidor MS DNS de modo automático, baseado na importação do AD configurada, porém algumas vezes é desejado ter um endereço diferente do servidor MS DNS. Ou até mesmo ter alguma redundâncias de servidores MS DNS. Neste caso, é possivel fazer toda essa configuração na página de configuração de importação do AD. Para ter mais de um servidor DNS basta separar por vírgulas.


.. note::

  Pode ser necessário permitir 'Atualização Dinâmica' nas propriedades da zona em seu servidor MS DNS para que o NxFilter possa atualizar os endereços IP dos seus terminais na zona do MS DNS.

Caso de uso 
************

 Ambiente Empresa ACME SA tem:
   #. Estações de trabalho  Windows, Linux e alguns notebooks Mac. Recentemente adquiriu diversos Chromebooks. 
   #. Usuários de smartphones iPhone e Android.
   #. Mais servidores Linux que mantém seu próprio website e compartilhamento de arquivos. 
   #. Funcionários que fazem serviço externo e usam notebooks [ Windows e Macbooks ]

 A Empresa ACME deseja controlar o acesso a internet dos usuários que estão dentro e fora do escritório, todos autenticando com sueus usuários no AD.

 Os procedimentos para uma completa implementação são:

 - Configurar e importar usuários e grupos do AD no NxFilter.
 - Preparar os terminais internos:
        - Windows - instalar o NxLogon
        - Linux - autenticação através da página de Login do próprio NxFilter
        - MacBooks - não trabalham com NxLogon, mas usam NxMapper. Então para atender essa demanda, basta instalar e rodar o NxMapper no DC do AD. 
 - Equipamentos para serviço externo:
        - NxClient nos notebooks. NxClient é, basicamente, um agente remoto para o NxFilter mas ainda tem a funcionalidade de SSO quando os dispositivos estiverem na empresa. 
 - Servidores - o melhor é não filtrar/controlar com o NxFilter e usar outro servidor DNS para eles ou - se ainda tiver interesse de usá-los com NxFilter - criar uma política que permita o acesso a tudo. Geralmente não é preciso bloquear nada em servidores. Vale lembrar que o servidor consumirá uma licença.
 - Smartphones ( iPhone e Androids ) - autenticação através da página de Login do NxFilter.
