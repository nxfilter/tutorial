Integração com o AD na nuvem com NxRelay
*****************************************************

É possível integrar o AD com um servidor na nuvem usando o NxRelay. Desde a v2.4, ao instalar o NxRelay em um controlador de domínio ele pode detectar e enviar para o servidor o usuário registrado no AD. Tanto o NxFilter quanto o NxCloud permitem a inegração com o AD através do NxRelay.

.. note::
  NxRelay é um retransmissor para o servidor DNS que foi desenvolvido inicialmente a fim de permitir que o NxCloud possa identificar usuários que se autenticassem em uma rede privada.

Implementando a integração do AD com o serviço na nuvem.

1. Instale o NxRelay em um controlador de domínios
   Afim de permitir que o NxRelay indetifique usuário autenticado é preciso que ele esteja instalado em um DC. Em todo caso, ainda pode haver conflitos de portas já que o DC tem um servidor MS DNS. Para evitar isso é indicado se adicionar um IP adicional no servidor e vincular o servidor MS DNS para um IP e o NxRelay para outro.

2. Use o servidor DNS do AD como seu servidor DNS Local.
   Em uma rede com AD é um requisito que o serviço DNS esteja funcionando. Para evitar falhas na integração com o AD deve-se usar o servidor MS DNS como **'Local DNS'** ou usar o NxRelay e fazer com o que o NxRelay use o servidor MS DNS como **'Local Domain'**.

3. Importando os usuários e grupo do AD para a nuvem.

   Como ocorre na instalação local do Servidor NxFilter, é necessário fazer a importação dos usuários e grupos para o servidor NxFilter. Considerando que o servidor NxFilter está na nuvem, é preciso deixar abertas portas TCP para que haja comunicação com o controlador do AD para que possa ser feita a importação.

.. note::
  A porta TCP/389 é a porta padrão usada para a importação, porém a porta pode ser alterada na página de configuração do NxFilter.

É possível integrar o AD usando recursos da nuvem em larga escala usando o NxRelay é conjunto com o NxFilter porém quando se utiliza o NxCloud o resultado é um pouco diferente. 

No NxCloud não há suporte a importação de usuários do AD. Então a integração ainda não é possível. Ele permite que o usuário do AD seja identificado através de um **'token_username'** nos registros de log, por isso consegue se identificar quem é quem. Porém é preciso criar o usuário manualmente se deseja aplicar políticas em um usuário do AD.

Por exemplo: Se existe no AD o usuário 'john100' e instalou o NxRelay com o token de usuário 'myrelay' na GUI do operador do NxCloud. Será possível identificar a requisição DNS do usuário 'john100' sob o nome 'myrelay_john100'. Mas se desejar aplicar uma política diferente a 'john100', é necessário que se crie o usuário 'john100' na GUI do operador.

.. note::

  No NxCloud, a identificação do usuário registrado no AD vem antes da associação com o IP.
