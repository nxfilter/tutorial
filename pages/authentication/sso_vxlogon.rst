********************************************
Single Sign-On com AD usando VxLogon
********************************************

O VxLogon é uma versão no formato de Script baseada no NxLogon. Em alguns casos houveram ambientes com problemas com o NxLogon, já que ele é um arquivo EXE e alguns antivírus acabam retornando falsos positivos com relação a ele. Por esse motivo foi criada essa versão do NxLogon usando VBScript. Ele mais simples e rápido. VxLogon é suportado pelo NxFilter a partir da versão 4.1.8.

Rodando
^^^^^^^^ 
Para executá-lo é necessário registrar o script 'vxlogon.vbs', contido no pacote do VxLogon, como um script de logon do Windows através de GPO. No lado do NxFilter, você precisa ativá-lo em 'Config > VxLogon'.

.. note::

 Caso não tenha trabalhado ainda com GPO e scripts de logon, veja como fazer em :ref:`Single Sign-On com AD usando NxLogon <sso_nxlogon>`. 

 Diferente do NxLogon, o VxLogon não precisa que seja informado o IP do Servidor.

Restrições de Segurança
^^^^^^^^^^^^^^^^^^^^^^^ 

 O VxLogon utiliza o protocolo DNS, tornando as coisas muito mais fáceis e simples. Porém, alguns usuários poderiam tentar burlar o sistema se autenticando com outro usuário para ter permissões com menos restrições já que os comandos utilizados ficam visíveis em um script. Para evitar esse tipo de problema, foi adicionado um procedimento de segurança durante a ativação do VxLogon.

 Então o VxLogon utiliza o protocolo DNS para estabelecer a comunicação com o servidor NxFilter e foram criados domínios únicos para efetuar o Logon e o Logoff.

 Em 'Config > VxLogon', há os parâmetros:

  - Logon Domain : vxlogon.example.com

  - Logoff Domain: vxlogoff.example.com

 É permitida a alteração desses domínios de acordo com a necessidade do ambiente. Após a alteração desses parâmetros no servidor é preciso alterar os arquivos a seguir no pacote do VxLogon para conter o novo endereço:

  - vxlogon.vbs

  - vxlogoff.bat

.. warning::

  Como é utilizado o comando 'nslookup' é importante manter o ponto final na escrita do domínio.


Análise/Testes do script
^^^^^^^^^^^^^^^^^^^^^^^^

  Rodar o script 'vxlogon.vbs' diretamente no CMD não trará nenhum retorno por causa do programa padrão utilizado pela engine VBScript, o 'wscript'. Mas caso deseje debugar o script, isso pode ser feito utilizando o 'cscript' 

.. code-block:: bash

  cscript vxlogon.vbs


E no servidor execute o NxFilter sem o modo daemon, dessa maneira também poderá visualizar as ocorrências do lado do servidor enquanto testa o script 'vxlogon'.
