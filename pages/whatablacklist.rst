.. _whatablacklist:

**********************************
Categorias - Classificação de Domínios
**********************************

.. toctree::
   :titlesonly:

   whatablacklist/whatblacklist.rst
   whatablacklist/using_jahaslist.rst
   whatablacklist/blacklist_recat.rst
   
