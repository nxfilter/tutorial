.. _custom_before:

******************************************************
Customização ou melhorias do NxFilter e seus clientes
******************************************************

*Antes de modificar o NxFilter

.. warning::

  Se há o desejo de fazer melhorias no NxFilter para sua ferramenta comercial, use NxFilter ou NxCloud.


Agora vamos falar sobre como personalizar NxFilter e seus aplicativos com sua própria marca. Antes de qualquer coisa, iremos demonstrar como padronizar a GUI. E depois falaremos sobre os outros componentes pois você pode se interessa.

Por último mostraremos como customizar os aplicativos do NxFilter.

.. toctree::
   :titlesonly:

   custom_before/custom_directory.rst
   custom_before/custom_dao.rst
   custom_before/custom_javadoc.rst
   custom_before/custom_oem.rst
   custom_before/custom_templates.rst
   custom_before/custom_other.rst
   custom_before/custom_package.rst
