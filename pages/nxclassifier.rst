.. _nxclassifier:

**************
NxClassifier
**************


.. toctree::
   :titlesonly:

   nxclassifier/what.rst
   nxclassifier/gui.rst
   nxclassifier/rule.rst
   nxclassifier/merge.rst

