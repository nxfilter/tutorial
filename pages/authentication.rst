.. _authentication:

****************
Autenticação
****************

O NxFilter provê diversas formas de autenticação, incluindo Single Sign-On integrado com AD

.. toctree::
   :titlesonly:

   authentication/nxfilter_auth.rst
   authentication/ad-integration.rst
   authentication/sso_nxlogon.rst
   authentication/sso_vxlogon.rst
   authentication/sso_nxmapper.rst
   authentication/sso_cxlogon.rst
   authentication/sso_802.rst
   authentication/custom_login.rst
   authentication/auth_order.rst

   
