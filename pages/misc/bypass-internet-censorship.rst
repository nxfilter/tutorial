DNS over HTTPS - Se livrando da censura
***************************************

Atualmente a Censura a Internet tem sido algo comum. Quando se utiliza servidores DNS publicos ou de um ISP há a possibilidade de que suas requisições DNS estejam sendo censuradas.

Há ainda questionamentos acerca de que alguns desses serviços estejam sendo controlados por autoridades sob a aplicação de leis e se aproveitando de suas informações de tráfego.

Para quem deseja uma internet sem esse tipo de controle foi adicionado o recurso **DNS over HTTPS**  a partir da versão 4.2.6 do NxFilter. É possível utilizar o DNS Cloudflare ou o HTTPS do Google como servidor DNS Upstream. A partir da ativação desse recursos todas as consultas/requisições DNS do NxFilter estarão criptografadas. Protegendo as solicitações vindas da rede interna.
É possível habilitar o recurso **DNS over HTTPS** acesse **DNS > Setup**. Mais informações sobre as opções podem ser vistas em :doc:`../gui_overview/gui_dns`.
