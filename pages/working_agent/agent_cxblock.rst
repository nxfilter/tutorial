.. _cxblock:

****************************************************
CxBlock para filtro no Chromebook
****************************************************
 
CxBlock é um agente remoto para o Chromebook. Antes era conhecido como 'NxBlock' mas por alterações no protocolo ele deve de ser renomeado. O NxBlock não terá mais a possibilidade de usar um WebSocket por conta de problemas com o servidor web embarcado no NxFilter, por isso ele será desativado e substituido pelo CxBlock o mais breve possível.

 .. image:: /images/nxblock_cxblock.png

---------------------
Instalando o CxBlock
---------------------

CxBlock é uma extensão do Chrome. Você pode instalá-lo pelo Chrome Web Store. É possível fazer o download através do link abaixo.

    - `Download CxBlock a paritr da Chrome Web Store`_

.. target-notes::
.. _`Download CxBlock a partir da Chrome Web Store`: https://chrome.google.com/webstore/detail/cxblock/jgmlagepgchaedmnjdabilicbjoleknd

-------------------------------
Aplicando filtros com o CxBlock
-------------------------------

As configurações do CxBlock podem ser feitas em '''Policy > Chromebook'''

-------------------------------
Conexão com o NxFilter
-------------------------------

Ao término da instalação, é preciso definir os parâmetros para comunicação da extensão com o NxFilter,

  - Server IP : O Endereço do NxFilter ( em caso de ser um cluster, o master )
  - Login Token : O Token do usuário ( pode ser obtido na tela de configuração do usuário )

Após a configuração dos parâmetros você pode testar a conectividade clicando no botão 'Test'. E então pressione 'Save' e o sistema já receberá as configurações.

--------------------------------
Protegendo sua configuração
--------------------------------

Pode-se controlar o acesso a página de configuração do CxBlock, limitando ao acesso somente com senha. Uma vez definida a senha de acesso e ativada, os usuários comuns serão impedidos de acessar a página do CxBlock e a área 'chrome://extension'.


.. note::
   É possível usar a 'Client Password' em 'Config > Admin' para acessar a configuração do CxBlock assim que estiver conectado ao servidor.

   Se estiver usando o NxCloud, você pode usar a senha do operador.


--------------------------------------
Identificando o usuário
--------------------------------------

O 'Login Token' e o Google Account são usados para identificar os usuários. Suponhemos que foi criado um usuário 'estudante' no NxFilter e foi feita a instalação de 10 CxBlock em 10 ChromeBooks usando o token desse usuário. Se o usuário dos Chromebook não se autenticarem no Google eles serão identificados no NxFilter como 'estudante', mas se um dele se autenticar no Google usando 'jose1234123@gmail.com', por exemplo, então o usuário aparecerá nos registros do NxFilter como 'estudante_jose1234123'.

-----------------------------------------------
Configuração em massa / Diversos dispositivos
-----------------------------------------------

Ao fazer a instalação do CxBlock em massa, seria muito trabalhoso configurar cada uma das estações. Visando facilidar esse processo existe uma forma de definir os parâmetros de modo centralizado.


