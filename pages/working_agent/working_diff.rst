***************************
Diferenças entre os agentes
***************************

NxFilter tem diversos agentes, com atribuições diferentes. Alguns são para single sign-on com Active Directory. Outros para filtro de usuários remotos e atualização de IP dinâmico. Outros podem ser usados para controle de aplicações e proxy filtering.

1. NxLogon e VxLogon

  Agente Single Sign-On para Active Directory. Você pode inicializá-lo com um script de logon através de uma GPO. O VxLogon é uma versão do NxLogon em Script.

2. NxMapper

 Outra opção para Single Sign-On com Active Directory. Mas diferente do NxLogon você o instala no Controlador de Domínio.

3. NxClient

 Agente para filtro de usuário remoto do NxFilter. Para casos de dispositivos móveis ou de redes distintas você pode instalar o NxClient em seus laptops.

4. NxUpdate

 Atualiização dinâmica de IP para NxFilter.

5. NxForward

 Redirecionador de páginas em HTTPS que tenham sido bloqueadas, extensão para o Chrome
