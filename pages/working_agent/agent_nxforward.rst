.. _nxforward:

****************************************************
NxForward para exibir a página de bloqueio em HTTPS
****************************************************
 
 No filtro DNS, quando é bloqueado um site HTTPS é recebido um alerta de SSL não seguro ao invés da página informando o motivo do bloqueio.

 .. image:: /images/nxforward_ssl.png


 É um erro comum pois o browser tenta protegê-lo do que é chamado de ataque 'Man in the Middle'. De qualquer forma ainda é um processo irritante pois na verdade é causado pela politica de filtragem. Há uma solução para esse caso quando se usa o navegador Chrome.

 Ao instalar o NxForward - que é uma extensão para o Google Chrome - as páginas HTTPS que forem bloqueadas serão redirecionadas para uma página HTTP novamente, permitindo assim a visualização da página de bloqueio em HTTP.

 O NxForward pode ser instalado a partir da Chrome Web Store. Baixe a partir do link.

    - `Download NxForward na Chrome Web Store`_

.. note::
   Pode parecer que ocultar esse tipo de informação é um processo perigoso. Porém o NxForward não oculta todos os avisos de problema com SSL. Ele tem uma inteligência embutida de modo a identificar quando o NxFilter realmente bloqueou o site HTTPS.

.. target-notes::
.. _`Download NxForward na Chrome Web Store`: https://chrome.google.com/webstore/detail/nxforward/ohhmhnionmgplhblinhpijfbaelmaojd 

