Propriedades OEM
********************

O arquivo 'oem.properties' para armazenar parâmetros específicos do  NxFilter e NxCloud. Se existir o arquivo 'oem.properties' em '/nxfilter/conf' com o seguinte valor.

.. code-block:: jproperties

   appname = MyFilter

1. NxFilter ou NxCloud adiciona o prefixo 'MYFILTER' nas mensagens Syslog.

2. Quando o NxFilter ou NxCloud envia um email de alerta para os usuários ele adiciona 'MyFilter' como prefixo do assunto.
