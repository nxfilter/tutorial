.. _gettingstarted:

***************
Iniciando
***************

.. warning::

   Essa documentação será descontinuada. 
   
   Durante muito tempo esta tradução não oficial do NxFilter ajudou muitas pessoas, serviu de apoio a diversos brasileiros e países em que a línguá oficial seja a portugues. Somado a isso contribuímos com a comunidade, mas atualmente existe uma tradução oficial, mantida pela empresa detentora do software e atualizada constantemente a qual pode ser acessada em <https://nxfilter.org/tutorial/a-why-nxfilter.php?locale=pt>


A forma mais rápida de iniciar o uso de NxFilter é seguindo esses passos

.. toctree::
   :titlesonly:

   getting_started/sys-req.rst
   getting_started/install.rst
   getting_started/update_nxfilter.rst
   getting_started/start_stop.rst
   getting_started/client.rst
   getting_started/ad_integration.rst
   getting_started/not_start.rst
   getting_started/cloud_install.rst

