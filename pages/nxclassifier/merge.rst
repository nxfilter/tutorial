*********************************************
Mesclando as listas de bloqueios da internet
*********************************************

É possível fazer o download e unir as listas públicas existentes na Internet junto com a Jahaslist e a Globlist de maneira automática, toda noite quando é feita a atualização das listas o sistema faz a junção com as URLs registradas em 'Classifier > Blocklist'.

.. note::

 A Globlist é uma fração da Jahaslist.


- O formato da lista de bloqueio : O arquivo deve ter os domínios separados por linhas. De um modo mais prático, todas as lista de bloqueio disponibilizadas em https://firebog.net funcionarão.

- Classificando por pontos: Podem haver domínios registrados em mais de uma categoria em diversas listas. Você pode fazer uma lista específica e assim mesclá-la definindo qual sua pontuação.

.. note::

   Quando uma URL de uma lista de bloqueio for excluída, os domínios existentes a partir dela serão removidos. Se você deseja remover uma uma lista sem causar problemas, classifique a pontuação de prioridade com ''-1'' e durante a junção que ocorre toda noite eles serão excluídos.

Mas deixamos claro que não é o objetivo usar na Jahaslist as listas de bloqueio definidas pelo usuário. Pois podem haver diversos falso positivos e domínios inexistentes nessas listas públicas, a Jahaslist contém domínios testados e validados, por esse motivo alguns dominios são removidos de acordo com as seguintes regras.

1. Se o domínio não existir.

2. Se já houver o domínio na Jahaslist.

3. Se ele estiver na lista de 100.000 domínios.

.. note::

   Ainda sim podem ocorrer de serem adicionados domínios já registrados na Jahaslist, pois o processo de verificação só identifica registros que forem completamente iguais, isso por que um processo de análise mais profundo iria causar impacto no sistema.
