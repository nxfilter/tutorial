****************************************
O que é uma classificação de domínios?
****************************************

Esta é uma parte essencial para que um filtro DNS possa bloquear os sitesi se baseando em categorias. O NxFilter tem algumas formas de fazer essa classificação:

1. Jahaslist

  Jahaslist é utilizado por padrão no NxFilter para fazer a classificação de domínios. Ela também tem suporte a classificação dinâmica dos sites através da engine NxClassifier, que é um 'motor' de classificação automática de sites que fica integrado ao NxFilter quando usa a Jahaslist.

  Para mais informações sobre a funcionalidade do NxClassifier e como adicionar registros à Jahaslist leia a seção :doc:`../nxclassifier`.

.. note::

  Junto com o pacote do NxFilter é disponibilizada uma licença de testes de 30 dias sem limite de dispositivos e uma licença de uso para 20 dispositivos. Uma vez instalado o NxFilter é ativada uma licença ilimitada por 30 dias, após isso é ativado o pacote da licença para 20 dispositivos.

2. Cloudlist

   É o nosso serviço terceirizado hospedado na nuvem que você pode contratar. Provê a classificação de mais de 30 milhões de domínios de forma prévia e ainda faz classificação dos demais de maneira dinâmica. Além de todas as vantagens, como o serviço está na nuvem você não precisa se preocupar com a disponibilidade, atualização ou importação.

.. warning::

  Desde a versão 4.1.3, o NxFilter vem com uma licença de teste de 30 dias da Cloudlist para 50 dispositivos.

3. Globlist

   Globlist é a uma nova blacklist, disponível desde o NxFilter v4.2.0.  Essa lista tem mais de 400 mil domínios, divididos em 3 categorias que são: ADS, Phishing/Malware e Pornô. A atualização é feita automaticamente. Globlist permite o uso de apenas um nível de política.

