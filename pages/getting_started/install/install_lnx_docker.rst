Usando o `Docker`_
--------------------------------

Charles Gunzelman fez imagens Docker para o NxFilter. Ele as disponibilizou no seguinte endereço:

 `Charles Gunzelman's NxFilter Docker images`_

Para usar o NxFilter como um container os parâmetros abaixo podem ser utilizados para dispor os serviços:

::

 docker run -dt \
 --name nxfilter \
 -v nxfilter-conf:/nxfilter/conf \
 -v nxfilter-log:/nxfilter/log \
 -v nxfilter-db:/nxfilter/db \
 -p 53:53/udp \
 -p 80:80 \
 -p 443:443 \
 -p 19002-19004:19002-19004 \
 packetworks/nxfilter-base:latest

.. target-notes::
.. _`Charles Gunzelman's NxFilter Docker images`: https://github.com/packetworks/docker-nxfilter
.. _`Docker`: https://www.docker.com/
