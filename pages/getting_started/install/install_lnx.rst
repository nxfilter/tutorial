Outras distribuições Linux
------------------------------------------------


.. note:: Em geral, quando se instala o NxFilter em sistemas Linux

  * É preciso ter acesso como usuário ``root``
  * Ter o Java 7 ou superior instalado
  * Pode iniciar NxFilter em modo daemon usando o parâmetro '-d' ao executar o script ``startup.sh``

.. warning::

  A versão 9 do OpenJDK causa instabilidades no servidor WEB do NxFilter ( GUI ), por isso é recomendada a utilização da versão 8.

Instalando
^^^^^^^^^^

#. Baixe o arquivo ``nxfilter-x.y.y.z.zip`` a partir da área de Download.
#. Extraia o arquivo zip na pasta ``/nxfilter``
#. Entre na pasta ``/nxfilter/bin`` e execute ``chmod +x *.sh``
#. Execute ``startup.sh``


.. code-block:: bash

  $ wget -t0 -c http://www.nxfilter.org/download/nxfilter-x.y.y.z.zip
  $ mkdir /nxfilter && cd /nxfilter
  $ unzip ~-/nxfilter-x.y.y.z.zip
  $ chmod +x /nxfilter/bin/*.sh
  $ /nxfilter/bin/startup.sh


.. image:: /images/linux_startup.png

* Para acessar a GUI de administração, entre com o endereço ip do servidor NxFilter no browser, 'http://<ip_servidor>/admin'. Usuário e senha inicial são 'admin' e 'admin'.

.. note::
   Você pode inicializar o NxFilter automáticamente usando o arquivo '/etc/rc.local'. Considerando que o NxFilter fora instalando em '/nxfilter', basta adicionar a linha '/nxfilter/bin/startup.sh -d' no arquivo '/etc/rc.local'. Não esqueça do parâmetro '-d' pois ele coloca o serviço NxFilter como daemon.
