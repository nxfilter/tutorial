Instalando com RPM 
--------------------------------

Rob Asher criou os pacotes RPM para o NxFilter. Ele distribui os pacotes no site abaixo:
    _`Rob Asher's NxFilter-RPMs`: https://bitbucket.org/DeepWoods/nxfilter-rpms

Para instalar o NxFilter usando o comando 'yum', instale antes o repositório do Rob Asher,

::
 yum install http://deepwoods.net/repo/deepwoods/deepwoods-release-6-2.noarch.rpm 

Assim, será possível instalar o NxFilter conforme abaixo,

::
 yum install nxfilter

Há também outros pacotes no repositório.

::
 yum install nxcloud

 yum install nxrelay

.. note:: Esses pacotes verificam todas as dependências para o NxFilter, inclusive o Java.
