Usando o Ubuntu
--------------------------------

É disponibilizado o pacote 'deb' para o Ubuntu Linux na área de Downloads.
Para instalá-lo, primeiro instale o Java. Baixo o pacote através do comando ``wget`` e então instale usando o comando ``dpkg``. Feito isso bastará iniciá-lo usando o script instalado através com o pacote.

 .. note::

  * No Youtube há o `Tutorial`_ .

 .. warning::

   * O OpenJDK 9 pode causar instabilidade no servidor web ( responsável pela GUI ), por isso é recomendado o uso do OpenJDK 8.

Instalando
^^^^^^^^^^

::

  $ sudo apt-get install openjdk-8-jre
  $ wget http://www.nxfilter.org/download/nxfilter-4.3.3.3.deb
  $ sudo dpkg -i nxfilter-4.3.3.3.deb
  $ sudo systemctl enable nxfilter.service
  $ sudo systemctl start nxfilter.service

Para acessar a interface administrativa, abra o seu browser de preferëncia e entre o endereço "http://<ip_servidor_nx>/admin". Se, por exemplo, você instalou o servidor NxFilter em uma máquina com o IP '192.168.100.2' digite "http://192.168.100.2/admin". 

 .. note:: No primeiro login o usuário é ``admin`` e a senha ``admin``.

Atualizando
^^^^^^^^^^^

O procedimento de atualização é bem simples, garanta que o serviço do NxFilter esteja parado.::

   $ sudo systemctl stop nxfilter.service
   $ sudo dpkg -i nxfilter-4.3.3.3.deb
   $ sudo systemctl start nxfilter.service


Removendo
^^^^^^^^^^

::

  sudo dpkg -r nxfilter


A partir do Ubuntu 18, o serviço ''systemd-resolved'' usa a porta UDP/53. Antes de instalar o NxFilter você deve desativá-lo. Para isso execute

::

 sudo service systemd-resolved stop
 sudo systemctl disable systemd-resolved.service


Em versões do Ubuntu anteriores a 16.04, ele deverá estar usando o Upstart ao invés do SystemD. Para esses casos o procedimento de parada e inicialização do serviço deve ser:

::

   $ sudo stop nxfilter
   $ sudo start nxfilter

.. target-notes::
.. _`Tutorial`: https://www.youtube.com/watch?v=INQ4dRwE3PU

