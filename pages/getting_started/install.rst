.. _install:

*************
Instalando
*************


.. toctree::
   :titlesonly:

   install/sys-req.rst
   install/install_which.rst
   install/install_win.rst
   install/install_lnx_debian.rst
   install/install_lnx_ubuntu.rst
   install/install_lnx_docker.rst
   install/install-raspbian.rst
   install/install_lnx_rpm.rst
   install/install_lnx_alpine.rst
   install/install_lnx.rst
   install/install_win_manually.rst
