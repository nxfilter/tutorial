Recursos Mínimos Necessários
****************************

- Windows, Linux, FreeBSD ou qualquer outro OS que tenha suporte a Java 7 ou 8.
- 768 MB RAM.
- 4 GB de espaço disponível.
- Portas UDP/53, TCP/80, TCP/443.

.. warning::
 
 - OpenJDK 9 é incompatível com o servidor Web do sistema. É recomendado o uso do OpenJDK 8 para casos de uso do NxFilter com o OpenJDK.

 - Por padrão, o NxFilter usa 768 MB de memória. Esse é o requisito mínimo, então para ambientes com centenas de usuários isso pode não ser o bastante. Para saber como alocar mais recursos acesse: :doc:`Performance <../misc/performance>`
